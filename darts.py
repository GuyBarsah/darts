import os
import io
import json
from datetime import datetime, time

import darts_params


# ------------------ Darts Calculator ------------------ #

def printWelcome():
    print "*" * 34 + str(datetime.now().time())[:8] + "*" * 34
    print "*" * 29 + " W e l c o  m e ! " + "*" * 29
    print "*" * 30 + "Have a nice game!" + "*" * 30
    # if datetime.now().time() < time(12, 00) or datetime.now().time() > time(13, 00):
    #     if raw_input(
    #             "This is not lunch break...are you sure? enter yes to continue or anything else to exit: ") != "yes":
    #         exit(0)


def printTurnSum(players, i):
    print "*" * 75
    print "Turn {} finished!".format(i)

    res_list = [(p, players[p]['Current score']) for p in darts_params.players]
    res_list.sort(key=lambda tup: -tup[1])

    for idx, p in enumerate(res_list):
        print "[{2:>3}]\t{0} score: {1}".format(p[0], p[1], idx + 1)
    print "*" * 75


def printTurn(players, player, turn):
    maxScore = getMaxScore(players)
    player_score = getScore(players, player)
    is_final_turn = turn == darts_params.rounds

    if is_final_turn and (maxScore - player_score > 180):
        print "Sorry {}, you can't win the game...".format(player)

    lastTurnMsg = ". {} points needed to be the leader, G 0 0 d   L u c k !".format(
        maxScore - player_score) if is_final_turn and maxScore - player_score else ""
    print "Hey {}, Your score is: {} {}".format(player, player_score, lastTurnMsg)


def printLastTurn(players):
    res_list = [(p, players[p]['Current score']) for p in darts_params.players]
    res_list.sort(key=lambda tup: -tup[1])
    leader = res_list[0][0]
    score = res_list[0][1]

    print "*" * 26 + " It's the Final Turn ! " + "*" * 26
    print "*" * 20 + " Current Leader: {} with {} points! ".format(leader, score) + "*" * 20


def printFinalMsg(players):
    printTurnSum(players, darts_params.rounds)

    res_list = [(p, players[p]['Current score']) for p in darts_params.players]
    res_list.sort(key=lambda tup: -tup[1])
    leader = res_list[0][0]
    score = res_list[0][1]

    print "*" * 25 + "C o n g r a t u l a t i o n s {}! you are the winner!".format(leader) + "*" * 25  # get nick


def getMaxScore(players):
    scores = [score["Current score"] for score in players.values()]
    return max(scores)


def getScore(players, player):
    score = players[player]["Current score"]
    return score


if __name__ == '__main__':
    # init sequence
    # Check if data exists
    root_dir = os.path.dirname(os.path.realpath(__file__))
    data_file = os.path.join(root_dir, 'Scores.json')
    if not os.path.exists(data_file):
        empty_dict = dict()
        f_rep = io.open(data_file, 'w', encoding="utf-8")
        json_to_write = unicode(json.dumps(empty_dict, ensure_ascii=False, indent=4))
        f_rep.write(json_to_write)
        f_rep.close()

    with open(data_file) as ffile:
        players = json.load(ffile)

    for player_name in darts_params.players:
        if player_name not in players:
            new_player = dict()
            new_player['name'] = player_name
            new_player['Bullzeye'] = 0
            new_player['Global high score'] = 0
            new_player['Turn high score'] = 0
            new_player['Current score'] = 0
            players[player_name] = new_player

    # Save score
    f_rep = io.open(data_file, 'w', encoding="utf-8")
    json_to_write = unicode(json.dumps(players, ensure_ascii=False, indent=4))
    f_rep.write(json_to_write)
    f_rep.close()

    while True:
        printWelcome()

        for p in darts_params.players:
            players[p]["Current score"] = 0

        for i in range(darts_params.rounds - 1):
            for p in darts_params.players:
                idx = i + 1
                printTurn(players, p, idx)
                score_string = raw_input("enter {} score: \n".format(p))
                scores = [int(tscore) for tscore in score_string.split(' ') if tscore != '']
                for score in scores:
                    if score == 50:
                        print "Bullzeye!"
                        players[p]['Bullzeye'] = players[p]['Bullzeye'] + 1

                score_result = sum(scores)
                if score_result > players[p]['Turn high score']:
                    print "New per-Turn high Score!!"
                    players[p]['Turn high score'] = score_result
                players[p]["Current score"] = players[p]["Current score"] + score_result
            printTurnSum(players, idx)

        printLastTurn(players)
        for p in darts_params.players:
            printTurn(players, p, darts_params.rounds)
            score_string = raw_input("enter {} score: \n".format(p))
            scores = [int(tscore) for tscore in score_string.split(' ') if tscore != '']
            for score in scores:
                if score == 50:
                    print "Bullzeye!"
                    players[p]['Bullzeye'] = players[p]['Bullzeye'] + 1

            score_result = sum(scores)
            if score_result > players[p]['Turn high score']:
                print "New per-Turn high Score!!"
                players[p]['Turn high score'] = score_result
            players[p]["Current score"] = players[p]["Current score"] + score_result

        printFinalMsg(players)

        for p in darts_params.players:
            # Check for new global record
            pscore = players[p]["Current score"]
            gscore = players[p]['Global high score']
            if pscore > gscore:
                players[p]['Global high score'] = pscore
                print "Well Done! {} has set a new personal record, new record: {}".format(p, pscore)
            players[p]["Current score"] = 0

        # Save score
        f_rep = io.open(data_file, 'w', encoding="utf-8")
        json_to_write = unicode(json.dumps(players, ensure_ascii=False, indent=4))
        f_rep.write(json_to_write)
        f_rep.close()

        if raw_input("Play another game? (yes/no) ") != "yes":
            exit(0)
